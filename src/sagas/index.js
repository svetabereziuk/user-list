import { put, takeLatest, all, call } from 'redux-saga/effects';
const host = 'https://mysterious-plains-03237.herokuapp.com/';

function* getUsers() {
    const response = yield call(fetch, `${host}api/users`);
    const users = yield call([response, 'json']);

    yield put({ type: "USERS_RECEIVED", users });
}

function* removeUser (payload) {
    yield call(fetch, `${host}api/users/${payload.id}`, { method: 'DELETE' });  
    yield put({ type: "REMOVE_USER_SUCCESS", id: payload.id });
}

function* addUser (payload) {
    yield call(fetch, `${host}api/users`, 
    { method: 'POST', 
      body: JSON.stringify(payload.user), 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      } 
    });
    yield put({ type: "ADD_USER_SUCCESS", user: payload.user });
}

function* actionGetWatcher() {
     yield takeLatest('GET_USERS', getUsers)
}

function* actionRemoveWatcher() {
    yield takeLatest('REMOVE_USER', removeUser)
}

function* actionAddWatcher() {
    yield takeLatest('ADD_USER', addUser)
}

export default function* rootSaga() {
   yield all([
    actionGetWatcher(),
    actionRemoveWatcher(),
    actionAddWatcher()
   ]);
}