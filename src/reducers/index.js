import {
    USERS_RECEIVED,
    ADD_USER_SUCCESS,
    REMOVE_USER_SUCCESS
} from '../actions';

const initialState = {
    users: []
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
      case USERS_RECEIVED:
        return {
          ...state,
          users: action.users
        };
  
      case REMOVE_USER_SUCCESS:
        return {
          ...state,
          users: state.users.filter((user) => user.id !== action.id)
        };
  
      case ADD_USER_SUCCESS:
        return {
          ...state,
          users: [ ...state.users, action.user ]
        };
  
      default:
        return state;
    }
}
