export const REMOVE_USER = 'REMOVE_USER'
export const ADD_USER = 'ADD_USER'
export const GET_USERS = 'GET_USERS'
export const USERS_RECEIVED = 'USERS_RECEIVED'
export const ADD_USER_SUCCESS = 'ADD_USER_SUCCESS'
export const REMOVE_USER_SUCCESS = 'REMOVE_USER_SUCCESS'


export const addUser = (user) => {
    return {
      type: ADD_USER,
      user: user
    }
};

export const removeUser = (id) => {
    return {
        type: REMOVE_USER,
        id: id
    }
}

export const getUsers = () => {
    return {
        type: GET_USERS
    }
}

export const receivedUsers = (users) => {
    return {
        type: USERS_RECEIVED,
        users: users
    }
}
