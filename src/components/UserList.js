import React, { forwardRef, useState, useEffect  } from 'react';
import { connect } from 'react-redux';
import { addUser, removeUser, getUsers } from '../actions';
import MaterialTable from 'material-table';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import Remove from '@material-ui/icons/Remove';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

function UserList(props) {
  const { getUsers, users, removeUser, addUser } = props;    
  const columns = [
    { title: 'Id', field: 'id' },
    { title: 'Email', field: 'email' },
    { title: 'Name', field: 'name' },
    { title: 'Birth Year', field: 'birthYear', type: 'numeric' },
    { title: 'Birth Place', field: 'birthCity' },
  ]

  useEffect(() => {
    getUsers();
  }, []);

  const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
  };

  return (
    <MaterialTable
    icons={tableIcons}
    title="User List"
    columns={columns}
    data={users}
    editable={{
      onRowAdd: addedUser =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            addUser(addedUser);
          }, 600);
        }),
      onRowDelete: removedUser =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve();
            removeUser(removedUser.id);
          }, 600);
        }),
    }}
    options={{
      paging: false
    }}
  />
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    users: state.users
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    addUser: user => dispatch(addUser(user)),
    removeUser: id => dispatch(removeUser(id)),
    getUsers: () => dispatch(getUsers())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
